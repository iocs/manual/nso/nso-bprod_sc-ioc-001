require essioc
require beamprod, 0.12.0+0

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(beamprod_DIR)/beamprod.iocsh", "P=NSO-BProd, R=, PP=TD-M:Ctrl-SCE-1, LEBT_IRIS=LEBT-Iris:ID-Iris-01, MAGNETRON=ISrc-CS:ISS-Magtr-01, MEBT_CHP=MEBT-010:BMD-Chop-001")
