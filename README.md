# NSO-BProd:SC-IOC-001 IOC for Beam Production and Timing Tables

## Responsible

*    Arek Gorzawski [e-mail](mailto:arek.gorzawski@ess.eu)

## Used modules

*    [beamprod](https://gitlab.esss.lu.se/e3/wrappers/e3-beamprod)
